package com.meuprojeto.aplicacao.repositorio;

import org.springframework.stereotype.Repository;

import com.meuprojeto.aplicacao.entidade.Pessoa;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PessoaRepository extends JpaRepository<Pessoa,Long> {
	
}
