package com.meuprojeto.aplicacao.entidade;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.GenerationType;
import javax.persistence.Column;

@Entity
public class Pessoa {
	    @Id
	    @GeneratedValue(strategy=GenerationType.AUTO)
		private long id;
	    
	    @Column(nullable=false)
		private String nome;
	    
	    @Column(nullable=false)
		private String cpf;
	    
	    @Column(nullable=false)
		private String rg;
	    
	    @Column(nullable=false)
		private String dataNascimento;
	    
	    @Column(nullable=false)
		private String genero;
	    
	    @Column(nullable=false)
		private String estadoCivil;
	    
	    @Column(nullable=false)
		private String cep;
	    
	    @Column(nullable=false)
		private String uf;
	    
	    @Column(nullable=false)
		private String cidade;
	    
	    @Column(nullable=false)
		private String bairro;
	    
	    @Column(nullable=false)
		private String endereco;
	    
	    @Column(nullable=false)
		private Integer numero;
	    
	    @Column(nullable=false)
		private String complemento;
	    
	 
	    
		
		public long getId() {
			return id;
		}
		
		public void setId(long id) {
			this.id = id;
		}
		
		public String getNome() {
			return nome;
		}
		
		public void setNome(String nome) {
			this.nome = nome;
		}

		public String getCpf() {
			return cpf;
		}

		public void setCpf(String cpf) {
			this.cpf = cpf;
		}

		public String getRg() {
			return rg;
		}

		public void setRg(String rg) {
			this.rg = rg;
		}

		public String getDataNascimento() {
			return dataNascimento;
		}

		public void setDataNascimento(String dataNascimento) {
			this.dataNascimento = dataNascimento;
		}

		public String getGenero() {
			return genero;
		}

		public void setGenero(String genero) {
			this.genero = genero;
		}

		public String getEstadoCivil() {
			return estadoCivil;
		}

		public void setEstadoCivil(String estadoCivil) {
			this.estadoCivil = estadoCivil;
		}

		public String getCep() {
			return cep;
		}

		public void setCep(String cep) {
			this.cep = cep;
		}

		public String getUf() {
			return uf;
		}

		public void setUf(String uf) {
			this.uf = uf;
		}

		public String getCidade() {
			return cidade;
		}

		public void setCidade(String cidade) {
			this.cidade = cidade;
		}

		public String getBairro() {
			return bairro;
		}

		public void setBairro(String bairro) {
			this.bairro = bairro;
		}

		public String getEndereco() {
			return endereco;
		}

		public void setEndereco(String endereco) {
			this.endereco = endereco;
		}

		public Integer getNumero() {
			return numero;
		}

		public void setNumero(Integer numero) {
			this.numero = numero;
		}

		public String getComplemento() {
			return complemento;
		}

		public void setComplemento(String complemento) {
			this.complemento = complemento;
		}
		
		
}
