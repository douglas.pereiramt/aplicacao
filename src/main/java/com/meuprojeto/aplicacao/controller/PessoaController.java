package com.meuprojeto.aplicacao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.Optional;

import com.meuprojeto.aplicacao.entidade.Pessoa;
import com.meuprojeto.aplicacao.repositorio.PessoaRepository;


@RestController
public class PessoaController {
		@Autowired
		private PessoaRepository pessoaRepository;
		
		@RequestMapping(value="/pessoas", method=RequestMethod.GET)
		public List<Pessoa> getPessoas(){
			return pessoaRepository.findAll();
		}
		
		@RequestMapping(value="/pessoa/{id}", method=RequestMethod.GET)
		public ResponseEntity<Pessoa> getById(@PathVariable(value="id") long id) {
			Optional<Pessoa> pessoa = pessoaRepository.findById(id);
			if (pessoa.isPresent()) {
				return new ResponseEntity<Pessoa>(pessoa.get(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		}
		
		@RequestMapping(value="/pessoa", method=RequestMethod.POST)
		public Pessoa salvar(@RequestBody Pessoa pessoa) {
			return pessoaRepository.save(pessoa);
		}
		
		@RequestMapping(value="/pessoa/{id}", method=RequestMethod.PUT)
		public ResponseEntity<Pessoa> atualizar(@PathVariable(value="id") long id, @RequestBody Pessoa novaPessoa) {
			Optional<Pessoa> antigaPessoa = pessoaRepository.findById(id);
			if (antigaPessoa.isPresent()) {
				Pessoa pessoa = antigaPessoa.get();
				pessoa.setNome(novaPessoa.getNome());
				pessoa.setCpf(novaPessoa.getCpf());
				pessoa.setRg(novaPessoa.getRg());
				pessoa.setDataNascimento(novaPessoa.getDataNascimento());
				pessoa.setGenero(novaPessoa.getGenero());
				pessoa.setEstadoCivil(novaPessoa.getEstadoCivil());
				pessoa.setCep(novaPessoa.getCep());
				pessoa.setUf(novaPessoa.getUf());
				pessoa.setCidade(novaPessoa.getCidade());
				pessoa.setBairro(novaPessoa.getBairro());
				pessoa.setEndereco(novaPessoa.getEndereco());
				pessoa.setNumero(novaPessoa.getNumero());
				pessoa.setComplemento(novaPessoa.getComplemento());
				pessoaRepository.save(pessoa);
				return new ResponseEntity<Pessoa>(pessoa, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}	
			
		}
		
		@RequestMapping(value="/pessoa/{id}", method=RequestMethod.DELETE)
		public ResponseEntity<Object> deletar(@PathVariable(value="id") long id) {
			Optional<Pessoa> pessoa = pessoaRepository.findById(id);
			if (pessoa.isPresent()) {
				pessoaRepository.delete(pessoa.get());
				return new ResponseEntity<>(HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}	
		}
		
}
